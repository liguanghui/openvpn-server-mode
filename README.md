## 目的

验证openvpn多客户端模式

### 准备

```bash
# 安装openvpn软件包
yum -y install easy-rsa openvpn
# 启用IP包转发
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf && sysctl -p
```

### 配置

签发证书

```bash
./easyrsa init-pki
./easyrsa build-ca
./easyrsa gen-req vpn-server nopass
./easyrsa sign-req server vpn-server
./easyrsa gen-req client1 nopass
./easyrsa gen-req client2 nopass
./easyrsa gen-req client3 nopass
./easyrsa sign-req client client1
./easyrsa sign-req client client2
./easyrsa sign-req client client3
./easyrsa gen-dh
```

### net30模式

启动服务

```bash
# 在vpn-server上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/server/net30/server.conf -O server.conf
openvpn --config server.conf

# 在client1上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/client/client1.conf -O client.conf
openvpn --config client.conf

# 在client2上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/client/client2.conf -O client.conf
openvpn --config client.conf

# 在client3上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/client/client3.conf -O client.conf
openvpn --config client.conf
```

### subnet模式

启动服务

```bash
# 在vpn-server上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/server/subnet/server.conf -O server.conf
openvpn --config server.conf

# 在client1上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/client/client1.conf -O client.conf
openvpn --config client.conf

# 在client2上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/client/client2.conf -O client.conf
openvpn --config client.conf

# 在client3上
wget https://gitlab.com/liguanghui/openvpn-server-mode/-/raw/main/config/client/client3.conf -O client.conf
openvpn --config client.conf
```

## 站点到站点的VPN网络

vpn-server上启动服务

```bash
# 要在日志文件里打时间戳
sed -i 's/ --suppress-timestamps//g' /usr/lib/systemd/system/openvpn-server@.service
systemctl daemon-reload
cat > /etc/openvpn/server/server.conf << 'EOF'
dev tun
proto udp
local 172.16.0.1
port 1194
topology subnet
server 10.200.0.0 255.255.255.0
remote-cert-tls client
# duplicate-cn
user openvpn
group openvpn
persist-tun
persist-key
keepalive 10 60
ping-timer-rem
verb 7
daemon
log-append /var/log/openvpn.log
client-config-dir /etc/openvpn/client
client-to-client
route 172.16.10.0 255.255.255.0
route 172.16.20.0 255.255.255.0
route 172.16.30.0 255.255.255.0
push "route 172.16.0.0 255.255.255.0"
push "persist-tun"
push "persist-key"
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
371230b610ebb21082c6c9d6b33ce479
0373763e596a207d9505ba82d2f9cd00
02d0afff7a0dad5fb32e04e881215d39
9a707ce8057d7e211b0e528ecbded2f3
7588eaa555cad5e043f9a6ac02243d61
95cce2f3eaa319a77bf41fb48f7a5690
2bc63dc7e0d356c710fd5302ec944d70
ece2d60caee95a9e63dd7240ca570f9d
04ccfbaea207a8c25852e16184aa30f5
72107dd002d20e4df69d62f330bb4953
e8a2c26200f42438b2458114cc630e24
d7812a701a54d1fe963b240997887dce
fa43c6470e6093dd2e08e8bd191fc318
431452e91e04789dc533cbbc70420672
83915d3b2392d886de300146eae8277a
35aa3c8a549b35c08d0ca63183251f84
-----END OpenVPN Static key V1-----
</tls-crypt>
<ca>
-----BEGIN CERTIFICATE-----
MIIDSDCCAjCgAwIBAgIUOn5EavRhh0MjROvEX406VZDUDOkwDQYJKoZIhvcNAQEL
BQAwFTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQxMTFaFw0zNDAx
MDMwMjQxMTFaMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0EwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDvS15NQpMqFPCSkBbEYSYiYAeXkA8KvAFwDQ6fwgwo
9yctWk6c4AGNy7NG/rsaZjngUywPVftR6YXrWIo+iobPknSocjic/j7vAxCkbZAC
wQkWCtQgp1n3IoPGNB2wZQ7eZSX14xk3GhjsdupH7j7oPuC6hLxcoWEy1RYvoa1R
2OsXg4ng7vQNJaHDk5qyD/pZRv8IudqRRrDbwuTbKD2fmilO7e+BmjCsSs5VocaV
hGnqcb/j2TgesESfPjUMxQz4NxcgU9+Rcx+UQ3yDZwh3OB90A+qKF6sGe0UIVsR1
5M9TuZZCLalZELKhWRO7UxGDBStbdUqz18dl2omrR/mzAgMBAAGjgY8wgYwwHQYD
VR0OBBYEFN5mqGBGtsFPXPNnksgRE700HV0yMFAGA1UdIwRJMEeAFN5mqGBGtsFP
XPNnksgRE700HV0yoRmkFzAVMRMwEQYDVQQDDApPUEVOVlBOLUNBghQ6fkRq9GGH
QyNE68RfjTpVkNQM6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG
9w0BAQsFAAOCAQEA154qeblSiRR6s2bHD5JmXWZ4s+o8n3H9Gz46XBsq8WAMZjuC
XMlzocrpkFMM8r8rWtsAbGqMAkBf1lLCVGMKIHzayfBpRnJ5VOIFY7aESUqGfqCc
s2JMH9meo69cWnwcNPK4PPrgYAC3+UxV9YrvdwyeQUGvbh2ybIiW1M3j7BXBcGKE
/xq0eXoHrjQu2443YsXnwYmdCe7YAPs8QTp+un6FWWIhSIT6/rvmbW6MsndQTyj/
tXLDnqLUVFfM1l8LGqqpFx8Y8buf/1vMdSstnWi7+X/ZBe5nPDCdjQJUa4cWbkxD
MPj3c7Co9WIrCGW/K1PTY0VqUqzeua6DNSAcJw==
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIDbjCCAlagAwIBAgIRAMJUQ74NkU13BURxrl4gKr0wDQYJKoZIhvcNAQELBQAw
FTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQyMjRaFw0yNjA0MTAw
MjQyMjRaMBUxEzARBgNVBAMMCnZwbi1zZXJ2ZXIwggEiMA0GCSqGSIb3DQEBAQUA
A4IBDwAwggEKAoIBAQCmINl9m9xER5NnbIPBIySt5Z3tid0XMd37x4gLXSncmz6H
R7kvIC4AyXvrgnMN3/dIPk0dDC7octlC/XRoY0yGP8F/MhLvY+bGO3HzfNuJFTI4
LuwfhPjMLu8ti0q8mEBBCcTbWuXD0hnU4+L7rUmPn5rH43bWQOxqFAK+iJgE2aAq
FT2JI9bIcco2FkldlhLwZ8E9RoYidO08GWzbVLX//V0jbgy6mwhiZly9Oseh2jGp
GVWrQgAT4dmnuNZkcgQfL1A6qQUDE3vingJPwEWiGOhmsHMe+lBAZHNL/Z/17BqF
51C0FQ0OLkjzXTX13VxJtpcCy8LtYctBT4q7P0ZNAgMBAAGjgbgwgbUwCQYDVR0T
BAIwADAdBgNVHQ4EFgQUWWGvR2ohelcpChPdnnuwEQn/868wUAYDVR0jBEkwR4AU
3maoYEa2wU9c82eSyBETvTQdXTKhGaQXMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0GC
FDp+RGr0YYdDI0TrxF+NOlWQ1AzpMBMGA1UdJQQMMAoGCCsGAQUFBwMBMAsGA1Ud
DwQEAwIFoDAVBgNVHREEDjAMggp2cG4tc2VydmVyMA0GCSqGSIb3DQEBCwUAA4IB
AQDkyoWzeBl2z5m+QkXd3J/dAwsYkBqAFdJ9702rimGkhOiucLqRCNCpOv13WE8y
DG9S+jM1xsSkxdrYLFFbJo7Jvly9cy6poXa3G9HoBMALaStusrFmTOWDnSTMv32/
NwaN0xxg074Gzn9nkScqYwytNLdTFdOyVgpuSJWqh3170vZdYbrdI7MSxMYo48hj
lACCUMWDDdmBo4ZXpBf0mFmU9HypfMUWy5rYT9Hb7Z0Mm0xVagnmxi8P6QbqO+h2
bLh8PNv8/OiFAfnIsLwAEgvWsjAh/zYkDni+bMtGmBdPf/Ob14Qz87+e8LErNM+u
31IjoWdJx+Kv+IsWPXQvQOtE
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCmINl9m9xER5Nn
bIPBIySt5Z3tid0XMd37x4gLXSncmz6HR7kvIC4AyXvrgnMN3/dIPk0dDC7octlC
/XRoY0yGP8F/MhLvY+bGO3HzfNuJFTI4LuwfhPjMLu8ti0q8mEBBCcTbWuXD0hnU
4+L7rUmPn5rH43bWQOxqFAK+iJgE2aAqFT2JI9bIcco2FkldlhLwZ8E9RoYidO08
GWzbVLX//V0jbgy6mwhiZly9Oseh2jGpGVWrQgAT4dmnuNZkcgQfL1A6qQUDE3vi
ngJPwEWiGOhmsHMe+lBAZHNL/Z/17BqF51C0FQ0OLkjzXTX13VxJtpcCy8LtYctB
T4q7P0ZNAgMBAAECggEABArTP/Xa645nondG0sbteDhuGf7mHPpe/Ga5Lg2bnH0/
4urL3BRcX4iHi2N73oqPPyLE8uqo+YmnNQoVxbR6LYoU8JNiApExEaxrSWbFsMA4
nfAGWX39enpp4iHRimU/Vash2cnhwrIKDt68rIMQQdO7Cxdt0ijM7TKEyT4USjSM
sDGehnkGjcTX4w3nnvyJ8sQ+T0lhfu7GLUoNYqzcPjS9Bn9z37/kEmptbB2O5yMG
o48AzBCXp2L849/ZyJwYiDecRWuAFYPNkeKcVCf9e9YA1q6/8xrrX1FVzZdi8Sr9
v7FKeS3wg7xjMgheBe0GkvvcvkoTTam96CRWAcV2AQKBgQC+13kQmwpZbC8y48PE
/V9fnAF0ImyWcQD+UlscpcSHHZ0rWNI/oujB8V8K+uY7tbG0CjTQXXqJBuzbe8Kc
IWDZcQBWs30m5FlRVVbQEvvBf8X9ViYwKRZm2iHokscZiE+oOeVi2hIJL9NrBbDd
XUHUgo9lNoSnVW7h/PQbJ36zeQKBgQDe2U2cpdLnSwTk0q1FhFymmS9VGZ3gqNj/
0AVgJVz8ThkKUL09YZSNIoARWdM0saC2yMvZJVC21wVuFjli22F3C3wqzrMeK81d
Aov6WuWk6UF43hdS7ibwUWJBQ4Kj4KwIIH8DSa2Nch3SMgY8na7tqabPa0pG33yc
w5GsPgFAdQKBgQCIh9L/BExlKTajK7uVJMekuRF5Kl+RSYsyZ2zzk8yD9bXJmyPf
jwcCKVKbomAwe46EhYC/SQEvIxs08teJp3+IUIWzgXmZ52fW/jy7Y7lk96qH0ahE
cECsmIYFw2xZYeHLMpBfJjAdDnAscsfTtrIv/K/l8xj4NfC8EKH7A0hr0QKBgApq
T5VNrsC+odjr+8su94GV0T+B9f4FBjdWipnGxUTJhaQUx/NxdxbtkNy+vP9Oeftx
AQ4CD3asMAXIJiB3rmN49vGtPXPgijWZo98slmURvcyfXKm0lb9Pnm78b8OnAYNT
uXv22pSsy7YCPoZgvEdBUZmKIyuS0GknPsy1DSvtAoGBALL8h9XJhlZMMLJJGUN0
dtxPlDqfHAJg8MGCT4G/r0WXbdVJ+bbGH2uURmvvS+iK0CVH6ztgf4WxUncwr0aa
RJ9+RRV7VEo2MDMEWwjArSAzfwWNFU2x6CbGIaw/A/iMUvS3QrN6sO64R4EF4e2n
f3PbOm7m2F6tQAKE89fxq8uX
-----END PRIVATE KEY-----
</key>
<dh>
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEAngYiw32VVwxCzvdWnShP9p3TNEI/aLNqrGzH/HVeKHZpK/iNlmyD
vjn5K/nLv24GRl+TbubQ/pGRPVtBaBX3T+t9Jb8zuougdA4UCegM1KtinmpU+bI1
DbM7bd75VzB9mBMjo6F+Vc8AFIeKnf/+iWLHsdavU80p7FquLOaq0sFMPwjpilxC
1rcX3K+lf1ZId8hgHX3hgR+7MKRYEd97g8OI9soXNkas7YgsCxWG/WmEGf7Md2HS
/cYaweFO4qedTz1ZKgbidqvCy95EzhcrA5plyNkQFowQzWyrGuOYBrVPN3xis8o8
+t+yeu7AgO62RkueCKFD7ibO5eumJ6fYJwIBAg==
-----END DH PARAMETERS-----
</dh>
EOF
systemctl start openvpn-server@server
systemctl enable openvpn-server@server
# 创建每个客户端的专属配置文件
cat > /etc/openvpn/client/client1 << 'EOF'
iroute 172.16.10.0 255.255.255.0
push "route 172.16.20.0 255.255.255.0"
push "route 172.16.30.0 255.255.255.0"
EOF
cat > /etc/openvpn/client/client2 << 'EOF'
iroute 172.16.20.0 255.255.255.0
push "route 172.16.10.0 255.255.255.0"
push "route 172.16.30.0 255.255.255.0"
EOF
cat > /etc/openvpn/client/client3 << 'EOF'
iroute 172.16.30.0 255.255.255.0
push "route 172.16.10.0 255.255.255.0"
push "route 172.16.20.0 255.255.255.0"
EOF
# 对离开vpn网络的流量进行地址转换
iptables -t nat -A POSTROUTING -s 10.200.0.0/24 -o eth0 -j MASQUERADE
```

在client1上

```bash
# 要在日志文件里打时间戳
sed -i 's/ --suppress-timestamps//g' /usr/lib/systemd/system/openvpn-client@.service
systemctl daemon-reload
cat > /etc/openvpn/client/client.conf << 'EOF'
dev tun
proto udp
remote openvpn.lgypro.com 1194
# nobind
client
remote-cert-tls server
user openvpn
group openvpn
verb 7
daemon
log-append /var/log/openvpn.log
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
371230b610ebb21082c6c9d6b33ce479
0373763e596a207d9505ba82d2f9cd00
02d0afff7a0dad5fb32e04e881215d39
9a707ce8057d7e211b0e528ecbded2f3
7588eaa555cad5e043f9a6ac02243d61
95cce2f3eaa319a77bf41fb48f7a5690
2bc63dc7e0d356c710fd5302ec944d70
ece2d60caee95a9e63dd7240ca570f9d
04ccfbaea207a8c25852e16184aa30f5
72107dd002d20e4df69d62f330bb4953
e8a2c26200f42438b2458114cc630e24
d7812a701a54d1fe963b240997887dce
fa43c6470e6093dd2e08e8bd191fc318
431452e91e04789dc533cbbc70420672
83915d3b2392d886de300146eae8277a
35aa3c8a549b35c08d0ca63183251f84
-----END OpenVPN Static key V1-----
</tls-crypt>
<ca>
-----BEGIN CERTIFICATE-----
MIIDSDCCAjCgAwIBAgIUOn5EavRhh0MjROvEX406VZDUDOkwDQYJKoZIhvcNAQEL
BQAwFTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQxMTFaFw0zNDAx
MDMwMjQxMTFaMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0EwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDvS15NQpMqFPCSkBbEYSYiYAeXkA8KvAFwDQ6fwgwo
9yctWk6c4AGNy7NG/rsaZjngUywPVftR6YXrWIo+iobPknSocjic/j7vAxCkbZAC
wQkWCtQgp1n3IoPGNB2wZQ7eZSX14xk3GhjsdupH7j7oPuC6hLxcoWEy1RYvoa1R
2OsXg4ng7vQNJaHDk5qyD/pZRv8IudqRRrDbwuTbKD2fmilO7e+BmjCsSs5VocaV
hGnqcb/j2TgesESfPjUMxQz4NxcgU9+Rcx+UQ3yDZwh3OB90A+qKF6sGe0UIVsR1
5M9TuZZCLalZELKhWRO7UxGDBStbdUqz18dl2omrR/mzAgMBAAGjgY8wgYwwHQYD
VR0OBBYEFN5mqGBGtsFPXPNnksgRE700HV0yMFAGA1UdIwRJMEeAFN5mqGBGtsFP
XPNnksgRE700HV0yoRmkFzAVMRMwEQYDVQQDDApPUEVOVlBOLUNBghQ6fkRq9GGH
QyNE68RfjTpVkNQM6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG
9w0BAQsFAAOCAQEA154qeblSiRR6s2bHD5JmXWZ4s+o8n3H9Gz46XBsq8WAMZjuC
XMlzocrpkFMM8r8rWtsAbGqMAkBf1lLCVGMKIHzayfBpRnJ5VOIFY7aESUqGfqCc
s2JMH9meo69cWnwcNPK4PPrgYAC3+UxV9YrvdwyeQUGvbh2ybIiW1M3j7BXBcGKE
/xq0eXoHrjQu2443YsXnwYmdCe7YAPs8QTp+un6FWWIhSIT6/rvmbW6MsndQTyj/
tXLDnqLUVFfM1l8LGqqpFx8Y8buf/1vMdSstnWi7+X/ZBe5nPDCdjQJUa4cWbkxD
MPj3c7Co9WIrCGW/K1PTY0VqUqzeua6DNSAcJw==
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIDVDCCAjygAwIBAgIRAJTZv3KybSLilvn+fDuM+dwwDQYJKoZIhvcNAQELBQAw
FTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQzMTlaFw0yNjA0MTAw
MjQzMTlaMBIxEDAOBgNVBAMMB2NsaWVudDEwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQC1Rp9T0x9fWowKs3hPc9m8CX+EmzhuiFYhqfpDy93vuZ1/zXFw
U0XK4t6SAZDqkX5dCqgfELkhDkqSYO+80iRXU2SCKTFFBqMtosq27PM1VUBxac6F
FLkn5zqkEc5KnBC9TEQkhLtGmT+QBddwoh4BxZY1jMwhXQ8PVo7DgXKw0+ldDMFB
DOIsfejzYPyXmhdmygGCuBv6CUo6dPMyPXJq2vKiZmaF1r/9opaJoFs/WXDt4wQh
ry1KweQqs5mtv+HkRreOdtEc1hQLj7umV/iolS48VcGb0nFRtbfcY2msao5maJr0
47vGnao/tu3RPTNLaUTw0ZF/2hyFYZp0sNQxAgMBAAGjgaEwgZ4wCQYDVR0TBAIw
ADAdBgNVHQ4EFgQUzQ8L6/SKR6TDdffYWtm3ea2yHDcwUAYDVR0jBEkwR4AU3mao
YEa2wU9c82eSyBETvTQdXTKhGaQXMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0GCFDp+
RGr0YYdDI0TrxF+NOlWQ1AzpMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQE
AwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAbF602WT+Lb7s7AMzL80t+HZzV6OvjQOE
q/uxaX3s/WXwtMUItD+tz/5+9wfGaGhQOyXl3WJC4Mf6qjEuH8YudnYE/hmNE0yQ
iSGoF+D14zNVuBXtT13v2zuzcBx7AsCGtw2RAvxKw4VndkKyYvniQPLIJZocbG82
UqwqymFdEgjY1VSX85CLAqS82eIPRYsfFdE0WJdkfRZpgW0Yjhf+qgeh3sl2S43a
Vep+bcYkURWwjguA+V++umodfn6BhIxzSqmCa7A+ar6kxcgUloAR5fhSbBCjExeT
i1TR11t9nIlPK1j+Tg0nERuXQ0vPWNG2BBIRwC8fh4bAalc00slTCw==
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC1Rp9T0x9fWowK
s3hPc9m8CX+EmzhuiFYhqfpDy93vuZ1/zXFwU0XK4t6SAZDqkX5dCqgfELkhDkqS
YO+80iRXU2SCKTFFBqMtosq27PM1VUBxac6FFLkn5zqkEc5KnBC9TEQkhLtGmT+Q
Bddwoh4BxZY1jMwhXQ8PVo7DgXKw0+ldDMFBDOIsfejzYPyXmhdmygGCuBv6CUo6
dPMyPXJq2vKiZmaF1r/9opaJoFs/WXDt4wQhry1KweQqs5mtv+HkRreOdtEc1hQL
j7umV/iolS48VcGb0nFRtbfcY2msao5maJr047vGnao/tu3RPTNLaUTw0ZF/2hyF
YZp0sNQxAgMBAAECggEACzJDTqpIZ/DP8SyYPpFhaOejRyhOf3vklKwK0kYqEo+X
MUNvVr3l89E4aFyNZqzkLlClWqnYmA3pwDRu35g7zg2ktGoL6ioDSSMivH1SdkBU
5yG0XkGN6sTcOTGvrqmzWlB2wv9UZoNk9XskN9zjgpOBeBRBWsGEgnDZB4KcIrIR
ZiXtDz+LizQozNOUSBva1pI7ng1y7RZ0AcnsLW6pD6eE9u/EVeg5qYybCP9HzCRP
7dPqUVITJqbwITY3fpfeJdFcyJd0ZfDd4sohJXdFVKspZCnP4EfEOLr+cnFMi6M7
Vz7hYRAsRZ8vnJuyv1IpEjpZQZ0XCWYbP/CV8kWfmQKBgQDSKANbgR75V1VQ23ok
k9FNWJ0ebAl88t5UEKUA28clWjPOCTyNWDPjidTzUNt6J1rZ2xH+dGJg2kdXokcS
Xh6y6spEzQz41po14QtAwnMfg1wuaL7ttbgOSaJBMxtlTL28AX6GL7l3A/BvVZ9n
RQ6Vv1IO1e7SpvXTCAeZq48NDQKBgQDc0c7PYyeciNIz9Vi/2rWehvzHPkog2Axg
k5+cB6DoGP74bOOGdHyCjh3mKKHrIiCsi+Klvg5LZbYLIHYDOmC+dnnkGgjRrroy
k3V8G10pQgfYOddlpg9O+8WkNhaikkNX3xkIHU1ulou3n3nxIhNVI+bXaRXz7jTU
PlGL0JyCtQKBgBsUluIqsFAXBeJmdcXS2myqF4z9BPf539ZbUrfamj3g8r4BCLF6
BS2z7CHycGW8PoPGK/prCuZKGllbiub+A9ywTqIw/hPuq2538lhE9krARZehXcEJ
4o7MxEC8kjIqgmSAmMo3yiFg+5GNKf5HsspvHebVgHHam+C2rywJJGTdAoGAUInZ
czyH8wjYGglPQFJl0ZcMVSM76DTEdukA2ujRYDXVsQgOCDkuPHPXJd4GCHufDS/M
D4V9MzS6q95ADLdbF7yggniYZNnkoZO00vosBWNG9y9Jh5KEnNspX9Y2dT8Bfugo
+hOt7TrNZMCuuisif/gjNsfmMNzdudes6GDC5M0CgYEAmvp4l30ERrZgA1eDo/hl
txow+0/BV88omo6nmgLYGrxxofyvDoMB4Y0EXSCJu6echyKzkpf0i/8scCgb7eqS
rPJykgLwvH25Yo4pfFDgCAtRAftRzECdyVHMtFnyWlWT3qje1fgn1Qx3aYGvRltv
pGmwSnplLPcelfv5U21aCnY=
-----END PRIVATE KEY-----
</key>
EOF
systemctl start openvpn-client@client.service
systemctl enable openvpn-client@client.service
# 对离开vpn网络的流量进行地址转换
iptables -t nat -A POSTROUTING -s 10.200.0.0/24 -o eth0 -j MASQUERADE
```

在client2上

```bash
# 要在日志文件里打时间戳
sed -i 's/ --suppress-timestamps//g' /usr/lib/systemd/system/openvpn-client@.service
systemctl daemon-reload
cat > /etc/openvpn/client/client.conf << 'EOF'
dev tun
proto udp
remote openvpn.lgypro.com 1194
# nobind
client
remote-cert-tls server
user openvpn
group openvpn
verb 7
daemon
log-append /var/log/openvpn.log
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
371230b610ebb21082c6c9d6b33ce479
0373763e596a207d9505ba82d2f9cd00
02d0afff7a0dad5fb32e04e881215d39
9a707ce8057d7e211b0e528ecbded2f3
7588eaa555cad5e043f9a6ac02243d61
95cce2f3eaa319a77bf41fb48f7a5690
2bc63dc7e0d356c710fd5302ec944d70
ece2d60caee95a9e63dd7240ca570f9d
04ccfbaea207a8c25852e16184aa30f5
72107dd002d20e4df69d62f330bb4953
e8a2c26200f42438b2458114cc630e24
d7812a701a54d1fe963b240997887dce
fa43c6470e6093dd2e08e8bd191fc318
431452e91e04789dc533cbbc70420672
83915d3b2392d886de300146eae8277a
35aa3c8a549b35c08d0ca63183251f84
-----END OpenVPN Static key V1-----
</tls-crypt>
<ca>
-----BEGIN CERTIFICATE-----
MIIDSDCCAjCgAwIBAgIUOn5EavRhh0MjROvEX406VZDUDOkwDQYJKoZIhvcNAQEL
BQAwFTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQxMTFaFw0zNDAx
MDMwMjQxMTFaMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0EwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDvS15NQpMqFPCSkBbEYSYiYAeXkA8KvAFwDQ6fwgwo
9yctWk6c4AGNy7NG/rsaZjngUywPVftR6YXrWIo+iobPknSocjic/j7vAxCkbZAC
wQkWCtQgp1n3IoPGNB2wZQ7eZSX14xk3GhjsdupH7j7oPuC6hLxcoWEy1RYvoa1R
2OsXg4ng7vQNJaHDk5qyD/pZRv8IudqRRrDbwuTbKD2fmilO7e+BmjCsSs5VocaV
hGnqcb/j2TgesESfPjUMxQz4NxcgU9+Rcx+UQ3yDZwh3OB90A+qKF6sGe0UIVsR1
5M9TuZZCLalZELKhWRO7UxGDBStbdUqz18dl2omrR/mzAgMBAAGjgY8wgYwwHQYD
VR0OBBYEFN5mqGBGtsFPXPNnksgRE700HV0yMFAGA1UdIwRJMEeAFN5mqGBGtsFP
XPNnksgRE700HV0yoRmkFzAVMRMwEQYDVQQDDApPUEVOVlBOLUNBghQ6fkRq9GGH
QyNE68RfjTpVkNQM6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG
9w0BAQsFAAOCAQEA154qeblSiRR6s2bHD5JmXWZ4s+o8n3H9Gz46XBsq8WAMZjuC
XMlzocrpkFMM8r8rWtsAbGqMAkBf1lLCVGMKIHzayfBpRnJ5VOIFY7aESUqGfqCc
s2JMH9meo69cWnwcNPK4PPrgYAC3+UxV9YrvdwyeQUGvbh2ybIiW1M3j7BXBcGKE
/xq0eXoHrjQu2443YsXnwYmdCe7YAPs8QTp+un6FWWIhSIT6/rvmbW6MsndQTyj/
tXLDnqLUVFfM1l8LGqqpFx8Y8buf/1vMdSstnWi7+X/ZBe5nPDCdjQJUa4cWbkxD
MPj3c7Co9WIrCGW/K1PTY0VqUqzeua6DNSAcJw==
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIDVDCCAjygAwIBAgIRAMqklFE6ptVqhYmKvbmS2UAwDQYJKoZIhvcNAQELBQAw
FTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQzMjZaFw0yNjA0MTAw
MjQzMjZaMBIxEDAOBgNVBAMMB2NsaWVudDIwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQC/thhuPk8D1STKvr/cQzyR2x6xGrKmU22Qt3J9/FWcTlzb02Sl
CPLOMmIUa550cFyWzbH5gyDaPRj6JZtDwqa5H0pTQGS5o6EM48VqlwYEARPRSY4M
sGauoiq/gGXKR1273WRSCnfgoYIkJ9gvksqw1bzgnMD1HTe/Ca3UYK+Y+eOv13BQ
OY1HMPi1vn7VbyAVCrimiAE5aZtjzrN+TVWM2CyHS6IJ5EzCDVOH/2aZdkGpG0Z+
XLG1gHrbVajL9HpAr1ofhV12kotehMQ1CqpPj6OyPkeF/ANx/tY6Sy0llpDJsekh
6tkWt6+HzuW4PM2jdlhOy4HJHcWkHvE7CscNAgMBAAGjgaEwgZ4wCQYDVR0TBAIw
ADAdBgNVHQ4EFgQUJnLZcDjovOJNrknsODErszTJYAIwUAYDVR0jBEkwR4AU3mao
YEa2wU9c82eSyBETvTQdXTKhGaQXMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0GCFDp+
RGr0YYdDI0TrxF+NOlWQ1AzpMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQE
AwIHgDANBgkqhkiG9w0BAQsFAAOCAQEA4Dwgpf8iIAeeRYIo2Usq9A521X2jacEc
We5RkJPTohRn8DPwJYi+i8Yy6UychVN5qCxZfnYsCaTsTv/c3QSTz0KG9RhCyk8+
DyLZxwetkRY5/iXCGwFKJA0SOJJFfbRMtV/T0pUgNPmG4og4JmS6RkEEaOo/pu9P
7gtFc5l56jvAoGhfmhQeyd8iqCuPTLUvVzi1PbWWmYeL/YXXT72f1FuPVVilpDQX
4Rkfij4yMG0aSuni03R3+kY+SURZgqURylHrfkv2B1fMlcHCk3DSxvMH9vJaH22R
Tl3wj45CO7nZIkfq8uFsmQSanRrtRGNvvapn49YfxLzwzFTvj8Iatw==
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/thhuPk8D1STK
vr/cQzyR2x6xGrKmU22Qt3J9/FWcTlzb02SlCPLOMmIUa550cFyWzbH5gyDaPRj6
JZtDwqa5H0pTQGS5o6EM48VqlwYEARPRSY4MsGauoiq/gGXKR1273WRSCnfgoYIk
J9gvksqw1bzgnMD1HTe/Ca3UYK+Y+eOv13BQOY1HMPi1vn7VbyAVCrimiAE5aZtj
zrN+TVWM2CyHS6IJ5EzCDVOH/2aZdkGpG0Z+XLG1gHrbVajL9HpAr1ofhV12kote
hMQ1CqpPj6OyPkeF/ANx/tY6Sy0llpDJsekh6tkWt6+HzuW4PM2jdlhOy4HJHcWk
HvE7CscNAgMBAAECggEAHDyRHR26zBz28hDF2AakM3RzcRD2WrajhuLqL2G850yI
c01Pv9oRTysPFKRkPpC1JnYN/mrgqhxBI0jqNUEEhvQzoY70tv4BkNE0iNZNKcZR
2avMixRpT1Pt2W5rL6+yv+KkLcxCa1jGfxZKSnmDtqeIjgmwhvnCdfYSPxttiamV
CF6VfJRsTBeI0h0ddkeMNcHfmZVD1A+efdXpaaWc/e0evl4NmkgAWayQSWHLjnMO
9LCn6PVQshMvScufWcJXURlJGWnJZDhlFJTV2S8D/JeM8A/YIWU2Z863pLTjLDTE
Bi8CMS6vrpaw76VZ0iDVEH+0a7iidMVGmAo7BZ2tMQKBgQDGpDzKoOjRG2KFfkQY
Dr0CcL6/TvAb8HE4Nyyx/rvdqu4r+kJvMBP1DE/BbnVmi8GEIr8fmw9nMzthi9ug
HW/f7cr51NqrVhkTALUAjZkc1/zD/06bDTOtffii+RsmvSwSTrV7sAqBGX7auac2
vN/7W7+Kf1N1N9jZLjhr0Kz8XQKBgQD3EZF2Jfjyhn/PqHxNZf5YCGXZVSFnBgGb
0VEyVBRwMm29i80tCKxvuFC5IiZKLE7h7ncHObpJvG1IHtVBZKwGMVO+3d/Q2DQ3
TflkCEZugfZaY3O+LnLTAx41qCQdG3lHrtBQL+pVGrXDng7tVv0FzTGGbLXKsSQp
IB3UNObKcQKBgF7TBiYgfCNeNEDPjsgBOR8SHSyMDTj5JcMVK6Pom1vs/fdGZzJW
vCAteWE4TpjePEnMoHoA9nBoV39wWS5s/vvhsxRiFOTXm+6jVsRl0+KeBI63sJl5
yuKWEPwTeG+cvI4cvqvF1K3eMt1M5NLPhQijuqLttzRsrXq3gr2NBxcNAoGBAOal
mvL+Hfpiv1yfgHNM3PVX2aTbSVSBEe1hqfhe77mSCkqiAUeiSj/9hDZgf/hMhtN6
J9ox9QuzAR4L0q+6iLE7SV/sh23S+GQMwd21r/KoFtBo+Xs4p28AASq8LgKSTFU0
CDpjkNIkHj1hB7RfudO5rBs+zzNKzWQrHE2nmTCxAoGASorRBMj96eOZD+bZNSUq
Tm1Cs6xTSGM8DOkAJQhMKGoraHAbHW0O8j4q0ekQUEG906NCXW3zNw14giZEyu2u
Md2IIP55zz1AzdRmadKMYrZnbd2/iiLK9sTnn9vECJ45AQMLKnT+owy0CYX3I7NR
yxXH4jzwdp5EKrGbCaNgEoY=
-----END PRIVATE KEY-----
</key>
EOF
systemctl start openvpn-client@client.service
systemctl enable openvpn-client@client.service
# 对离开vpn网络的流量进行地址转换
iptables -t nat -A POSTROUTING -s 10.200.0.0/24 -o eth0 -j MASQUERADE
```

在client3上

```bash
# 要在日志文件里打时间戳
sed -i 's/ --suppress-timestamps//g' /usr/lib/systemd/system/openvpn-client@.service
systemctl daemon-reload
cat > /etc/openvpn/client/client.conf << 'EOF'
dev tun
proto udp
remote openvpn.lgypro.com 1194
# nobind
client
remote-cert-tls server
user openvpn
group openvpn
verb 7
daemon
log-append /var/log/openvpn.log
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
371230b610ebb21082c6c9d6b33ce479
0373763e596a207d9505ba82d2f9cd00
02d0afff7a0dad5fb32e04e881215d39
9a707ce8057d7e211b0e528ecbded2f3
7588eaa555cad5e043f9a6ac02243d61
95cce2f3eaa319a77bf41fb48f7a5690
2bc63dc7e0d356c710fd5302ec944d70
ece2d60caee95a9e63dd7240ca570f9d
04ccfbaea207a8c25852e16184aa30f5
72107dd002d20e4df69d62f330bb4953
e8a2c26200f42438b2458114cc630e24
d7812a701a54d1fe963b240997887dce
fa43c6470e6093dd2e08e8bd191fc318
431452e91e04789dc533cbbc70420672
83915d3b2392d886de300146eae8277a
35aa3c8a549b35c08d0ca63183251f84
-----END OpenVPN Static key V1-----
</tls-crypt>
<ca>
-----BEGIN CERTIFICATE-----
MIIDSDCCAjCgAwIBAgIUOn5EavRhh0MjROvEX406VZDUDOkwDQYJKoZIhvcNAQEL
BQAwFTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQxMTFaFw0zNDAx
MDMwMjQxMTFaMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0EwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQDvS15NQpMqFPCSkBbEYSYiYAeXkA8KvAFwDQ6fwgwo
9yctWk6c4AGNy7NG/rsaZjngUywPVftR6YXrWIo+iobPknSocjic/j7vAxCkbZAC
wQkWCtQgp1n3IoPGNB2wZQ7eZSX14xk3GhjsdupH7j7oPuC6hLxcoWEy1RYvoa1R
2OsXg4ng7vQNJaHDk5qyD/pZRv8IudqRRrDbwuTbKD2fmilO7e+BmjCsSs5VocaV
hGnqcb/j2TgesESfPjUMxQz4NxcgU9+Rcx+UQ3yDZwh3OB90A+qKF6sGe0UIVsR1
5M9TuZZCLalZELKhWRO7UxGDBStbdUqz18dl2omrR/mzAgMBAAGjgY8wgYwwHQYD
VR0OBBYEFN5mqGBGtsFPXPNnksgRE700HV0yMFAGA1UdIwRJMEeAFN5mqGBGtsFP
XPNnksgRE700HV0yoRmkFzAVMRMwEQYDVQQDDApPUEVOVlBOLUNBghQ6fkRq9GGH
QyNE68RfjTpVkNQM6TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG
9w0BAQsFAAOCAQEA154qeblSiRR6s2bHD5JmXWZ4s+o8n3H9Gz46XBsq8WAMZjuC
XMlzocrpkFMM8r8rWtsAbGqMAkBf1lLCVGMKIHzayfBpRnJ5VOIFY7aESUqGfqCc
s2JMH9meo69cWnwcNPK4PPrgYAC3+UxV9YrvdwyeQUGvbh2ybIiW1M3j7BXBcGKE
/xq0eXoHrjQu2443YsXnwYmdCe7YAPs8QTp+un6FWWIhSIT6/rvmbW6MsndQTyj/
tXLDnqLUVFfM1l8LGqqpFx8Y8buf/1vMdSstnWi7+X/ZBe5nPDCdjQJUa4cWbkxD
MPj3c7Co9WIrCGW/K1PTY0VqUqzeua6DNSAcJw==
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIDVDCCAjygAwIBAgIRANhprhvWm6wNGmhULvLHpn0wDQYJKoZIhvcNAQELBQAw
FTETMBEGA1UEAwwKT1BFTlZQTi1DQTAeFw0yNDAxMDYwMjQzMzNaFw0yNjA0MTAw
MjQzMzNaMBIxEDAOBgNVBAMMB2NsaWVudDMwggEiMA0GCSqGSIb3DQEBAQUAA4IB
DwAwggEKAoIBAQC3oAUXQdsmBDRg2UfvKlCv1W8BjtsEJVdembabPoLpfnZCLnxa
iRWivgp/2lawkuBLc1/yeD0XcK8OtqGvE5fmIcDyeJQCaHrhgVlmxFxKu8Wq6brv
AoGJtXmdx/U6Do1DPtLc29cjvSgeGx1P+vaqX7umsldDGKhLXGecSfwojbKPdWza
ivSbJZrHuTg3gyEX1HTcdthg+G3IVsO1Zmyky4KS/OSNMQ6h1OF3J4iqSF45Oqex
8mlc3oplEsmfMnEQNqKUR/p5rlaVzHq12wAzu53jw6luLutXeSB5JSLCo8YMHP8X
mPEqGw1xAzHvaZLgveRLIkTGjb3Zux3JkXR3AgMBAAGjgaEwgZ4wCQYDVR0TBAIw
ADAdBgNVHQ4EFgQU8NA8fRfwkCK6iSLjf6VzbFF/6AEwUAYDVR0jBEkwR4AU3mao
YEa2wU9c82eSyBETvTQdXTKhGaQXMBUxEzARBgNVBAMMCk9QRU5WUE4tQ0GCFDp+
RGr0YYdDI0TrxF+NOlWQ1AzpMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQE
AwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAvbQOxSTl6cZoDOwRa/R6z7VS1Waplt3l
Evl6qO+XQZo+47ybCFc9563b+kZIMeeFl0Kp5P7juuKkZMkESWLqtFHhmcq+hk4Y
zqgijMKVgJeX5xeo28wfA27MpgXSoLV7MFnmfPesOZXI9dPJjMteJU/i+kE8OdEx
6cmvw3lZcC4CH5xES7fzm1Q9BGqWij+ClJw3UTlR+RlurOn5bTcGUBE+SG32dxw/
hz4irlbDt0xm2KofvKzbv4A6o1pLrJEEsxKWSbBVaCWrvitnzSx3l/ya4EgMTPqz
dJADowcTw1nP6GjwjZHHqPo7UodCQJpQ7Er/uNNn/WaafhZNjWKxyQ==
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC3oAUXQdsmBDRg
2UfvKlCv1W8BjtsEJVdembabPoLpfnZCLnxaiRWivgp/2lawkuBLc1/yeD0XcK8O
tqGvE5fmIcDyeJQCaHrhgVlmxFxKu8Wq6brvAoGJtXmdx/U6Do1DPtLc29cjvSge
Gx1P+vaqX7umsldDGKhLXGecSfwojbKPdWzaivSbJZrHuTg3gyEX1HTcdthg+G3I
VsO1Zmyky4KS/OSNMQ6h1OF3J4iqSF45Oqex8mlc3oplEsmfMnEQNqKUR/p5rlaV
zHq12wAzu53jw6luLutXeSB5JSLCo8YMHP8XmPEqGw1xAzHvaZLgveRLIkTGjb3Z
ux3JkXR3AgMBAAECggEAEuews9JImRi5uSGQXGPuPAwstOuw9N5QqSFeQYwlYkYY
2tDPu2p9sGxXHGQC+i7vqhT40zfkjPNBsrFu4rLmYqp6+PvHu++8fFca4zpi3iGI
RQPHW9rVh14IZWGJh9lTsJFZQvqHTdtTCWEf8B9rIeNf/dZCpwUfiT1m6sUF5w7H
0N+avmwLnO7JCnqbrP5CpwCakiTAryZzvIWD9E1pGqagA/y6Mw5C12aenyPUxBAZ
JYgdU125cJ5ROJXXA4ezSNn048mZ+C3X5dFAY8Du+AzezvKQ3fVipX3QEWkolCzU
9nhSGzC83c1SGLc3CTGsb89xyPak5P1h80yOd0h0IQKBgQD76+KEcQzWOm22PDR/
YQT7cHrO6jVpcRz9+vmRFRfIqJ/0b9dmIwdIMOAZPYxPDw6Ee2uHgzw1yn2dxfIv
f2qOnHYcwMn8WqtqRcw5SB2Qf91snP1hJyntQ+nGByWps94Zv0oACKQRu6X1NFtQ
Gk+QJqhab3Y7/urYuh7bynnoKQKBgQC6mRLVd0XwghDgum0+TcgLTNN65JjkyPzk
a8IAWUfVbq+j5GOow9aJZPy/tchNamA55dOr4BVnDqgu9WpS5R9R88SnzjzYPTgw
cSSe04xqIDu44lsk7ACX+2mBRpKByLr0sUsr2OWK4O5BqsLa3G9qwVKdCmyaO+Bz
OWqQaeyLnwKBgQC9/c5d+CyoPHSVpZ4qGu94+gGEn5ocpZQp7+fcBT5ktNe9r8OR
YgWR6lp4brSswzw9qh8UmmMEZmyKWskmDUA9wb5KPROYYIE6qt00rjCt0+EYkuV9
Kd4y9K8QDD/ZVq/AV0Xl9J5YSV8GKLBdHxq2KpF5cuqGoOBZBzVSqcd60QKBgD4D
48uIUXlGcP+PVgFP7n068ko0sU/QMKRl39sn0QTxPXSV0j1joOiW5J1+d+yqvwJf
DMNhvRs8Ns8sh0K3nXoqLyCqHXSsnAEViVsz6cXXoqGmsS4LNYNNblolr0ltAh2S
0u3mrxtB+E1gyFPe0TTEvYUt0rxLzCPfYDhQy+n3AoGAe5tO3SZseNNW2kk7vXym
M6BsPwSkML/bEHirNcV4fGq7g6dBSkUnlevbsATxn/zAQxMa4a9/iEWwxuroRE6R
531FKoH4Y6QneQE4irCehJHJJeuE8jTJZKHdvwVXQNemmgfKF7AyKmI78dahrWUB
s59CxHmAISHykkmfwfL1Ogs=
-----END PRIVATE KEY-----
</key>
EOF
systemctl start openvpn-client@client.service
systemctl enable openvpn-client@client.service
# 对离开vpn网络的流量进行地址转换
iptables -t nat -A POSTROUTING -s 10.200.0.0/24 -o eth0 -j MASQUERADE
```

验证

```bash
ping -c 3 -i 0.5 -W 1 10.200.0.1
ping -c 3 -i 0.5 -W 1 10.200.0.2
ping -c 3 -i 0.5 -W 1 10.200.0.3
ping -c 3 -i 0.5 -W 1 10.200.0.4

ping -c 3 -i 0.5 -W 1 172.16.0.1
ping -c 3 -i 0.5 -W 1 172.16.10.1
ping -c 3 -i 0.5 -W 1 172.16.20.1
ping -c 3 -i 0.5 -W 1 172.16.30.1

ping -c 3 -i 0.5 -W 1 172.16.0.100
ping -c 3 -i 0.5 -W 1 172.16.10.100
ping -c 3 -i 0.5 -W 1 172.16.20.100
ping -c 3 -i 0.5 -W 1 172.16.30.100
```

## 关于redirect-gateway的yongfa

以vpn1为例，在启动openvpn之前，路由如下

```bash
[root@client1 ~]# ip route
# 到默认网关
default via 172.16.10.253 dev eth0
# 不知道干嘛的，但和redirect-gateway无关
169.254.0.0/16 dev eth0 scope link metric 1002
# 到本地LAN
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1
```

在加入openvpn之后，由于openvpn服务器没有推送redirect-gateway配置，所以路由如下

```bash
[root@client1 ~]# ip route
default via 172.16.10.253 dev eth0
# tun0添加的网络路由
10.200.0.0/24 dev tun0 proto kernel scope link src 10.200.0.2
169.254.0.0/16 dev eth0 scope link metric 1002
# push "route"指令添加的到其他网段的路由
172.16.0.0/24 via 10.200.0.1 dev tun0
# 到本地LAN
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1
172.16.20.0/24 via 10.200.0.1 dev tun0
172.16.30.0/24 via 10.200.0.1 dev tun0
```

redirect-gateway是一个客户端选项，由服务器推送过来，用来指导客户端使用路由命令修改本地路由，把出向流量发送到vpn隧道。

在服务器配置`push "redirect-gateway"`时，客户端会做以下事情：

1. 为--remote指定的地址做一条/32路由，指向现在的网关，否则就会出现路由循环：我想到--remote地址，
   先到隧道入口，到了隧道入口，又要到--remote地址，又到隧道入口，...，vpn客户端很快就会发现连不上vpn服务器了，
   通过ping-exit或者ping-restart的配置触发重启。
2. 删除现在的默认路由。由于已经显式指定了到vpn服务器的路由，所有vpn链路没有问题。但是由于丢失默认路由，
   所有远程ssh连接、session manager连接都会断开，由于本地路由还在，可以使用同网段机器（本例中的app1）做跳板连接。
   vnc不依靠机器里的路由配置，总是可用。
3. 新增一个默认路由，将其指向route-gateway指向的地址，所有流量都会发往tun0隧道。客户端可以通过隧道上网。
   客户端到dns服务器的流量（100.100.2.136、100.100.2.138）也经由vpn服务器转发。

客户端使用的命令

```bash
Mon Jan  8 11:44:52 2024 us=135939 /sbin/ip link set dev tun0 up mtu 1500
Mon Jan  8 11:44:52 2024 us=139422 /sbin/ip addr add dev tun0 10.200.0.2/24 broadcast 10.200.0.255
Mon Jan  8 11:44:52 2024 us=141722 /sbin/ip route add 39.101.195.30/32 via 172.16.10.253
Mon Jan  8 11:44:52 2024 us=144190 /sbin/ip route del 0.0.0.0/0
Mon Jan  8 11:44:52 2024 us=145797 /sbin/ip route add 0.0.0.0/0 via 10.200.0.1
Mon Jan  8 11:44:52 2024 us=147430 /sbin/ip route add 172.16.0.0/24 via 10.200.0.1
Mon Jan  8 11:44:52 2024 us=148947 /sbin/ip route add 172.16.20.0/24 via 10.200.0.1
Mon Jan  8 11:44:52 2024 us=150608 /sbin/ip route add 172.16.30.0/24 via 10.200.0.1
```

客户端上的路由

```bash
[root@client1 ~]# ip route
# 原有的默认网关被取代，所有流量都通过tun0发往vpn服务器
default via 10.200.0.1 dev tun0
10.200.0.0/24 dev tun0 proto kernel scope link src 10.200.0.2
# 到--remote地址的路由，保证vpn链路的畅通
39.101.195.30 via 172.16.10.253 dev eth0
169.254.0.0/16 dev eth0 scope link metric 1002
172.16.0.0/24 via 10.200.0.1 dev tun0
# 到本地LAN的路由没有发生变化
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1
172.16.20.0/24 via 10.200.0.1 dev tun0
172.16.30.0/24 via 10.200.0.1 dev tun0
```

如果服务器配置的是`push "redirect-gateway local"`，就表示vpn客户端和vpn服务器在一个内网LAN里，可以直接通过链路路由访问，
所以可以省掉上面的第一步，就不用单独为--remote配置一条路由。在本例中，vpn客户端和服务器显然不在一个LAN里，进行这样的配置会中断vpn链路。
机器不能上网，也不能通过远程ssh连接、session manager登录，但是可以通过同网段机器做跳板，vnc总是可用连接。

客户端使用的命令

```bash
Mon Jan  8 12:23:42 2024 us=621121 /sbin/ip link set dev tun0 up mtu 1500
Mon Jan  8 12:23:42 2024 us=623567 /sbin/ip addr add dev tun0 10.200.0.2/24 broadcast 10.200.0.255
Mon Jan  8 12:23:42 2024 us=626024 /sbin/ip route del 0.0.0.0/0
Mon Jan  8 12:23:42 2024 us=629850 /sbin/ip route add 0.0.0.0/0 via 10.200.0.1
Mon Jan  8 12:23:42 2024 us=631785 /sbin/ip route add 172.16.0.0/24 via 10.200.0.1
Mon Jan  8 12:23:42 2024 us=633194 /sbin/ip route add 172.16.20.0/24 via 10.200.0.1
Mon Jan  8 12:23:42 2024 us=634570 /sbin/ip route add 172.16.30.0/24 via 10.200.0.1
```

客户端上的路由

```bash
[root@client1 ~]# ip route
default via 10.200.0.1 dev tun0
10.200.0.0/24 dev tun0 proto kernel scope link src 10.200.0.2
169.254.0.0/16 dev eth0 scope link metric 1002
172.16.0.0/24 via 10.200.0.1 dev tun0
# 到本地LAN的路由还在，所以可以通过同网段机器跳转
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1
172.16.20.0/24 via 10.200.0.1 dev tun0
172.16.30.0/24 via 10.200.0.1 dev tun0
```

客户端vpn隧道也是不能正常工作的，因为存在路由环路，日志可以证明这一点

```log
Mon Jan  8 12:25:54 2024 us=335662 TUN READ [60]
Mon Jan  8 12:25:54 2024 us=335691 MSS: 1460 -> 1358
Mon Jan  8 12:25:54 2024 us=335699 TLS: tls_pre_encrypt: key_id=0
Mon Jan  8 12:25:54 2024 us=335720 UDP WRITE [84] to [AF_INET]39.101.195.30:1194: P_DATA_V2 kid=0 DATA len=83
Mon Jan  8 12:25:54 2024 us=335738 TUN READ [112]
Mon Jan  8 12:25:54 2024 us=335746 Recursive routing detected, drop tun packet to [AF_INET]39.101.195.30:1194
```

如果服务器上配置了更智能的`push "redirect-gateway autolocal"`
，就可以自动判断--remote地址和自己是否在一个LAN里，如果在就表现得和使用local标签一样，
在替换默认路由前不添加--remote专用路由；如果不在一个LAN里，就和什么都不写一样，在替换默认路由前添加--remote专属路由，
保证vpn链路通畅。在本例中，vpn客户端和服务器显然不在一个LAN里，所以表现为后者。

客户端使用的命令

```bash
Mon Jan  8 12:37:23 2024 us=102766 /sbin/ip link set dev tun0 up mtu 1500
Mon Jan  8 12:37:23 2024 us=105128 /sbin/ip addr add dev tun0 10.200.0.2/24 broadcast 10.200.0.255
Mon Jan  8 12:37:23 2024 us=107498 /sbin/ip route add 39.101.195.30/32 via 172.16.10.253
Mon Jan  8 12:37:23 2024 us=111501 /sbin/ip route del 0.0.0.0/0
Mon Jan  8 12:37:23 2024 us=113287 /sbin/ip route add 0.0.0.0/0 via 10.200.0.1
Mon Jan  8 12:37:23 2024 us=114646 /sbin/ip route add 172.16.0.0/24 via 10.200.0.1
Mon Jan  8 12:37:23 2024 us=115794 /sbin/ip route add 172.16.20.0/24 via 10.200.0.1
Mon Jan  8 12:37:23 2024 us=117221 /sbin/ip route add 172.16.30.0/24 via 10.200.0.1
```

客户端上的路由

```bash
[root@client1 ~]# ip route
default via 10.200.0.1 dev tun0 
10.200.0.0/24 dev tun0 proto kernel scope link src 10.200.0.2 
39.101.195.30 via 172.16.10.253 dev eth0 
169.254.0.0/16 dev eth0 scope link metric 1002 
172.16.0.0/24 via 10.200.0.1 dev tun0 
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1 
172.16.20.0/24 via 10.200.0.1 dev tun0 
172.16.30.0/24 via 10.200.0.1 dev tun0
```

上面的几个标签都会删除原有的默认路由，新增一个默认路由。如果服务器设置的是`push "redirect-gateway def1"`
，就可以使用覆盖而不是替换的方式更新路由，具体来说，由于路由匹配使用最长前缀原则，而默认路由的前缀是0，所有我们只要设置一组前缀大于0的路由，这些路由的优先级更高，会先默认路由一步把流量引到vpn服务器，这样就可以不用删除默认路由。在客户端关闭阶段，只要删除这些新增的高优先级路由，默认路由就自动暴露出来了。

客户端使用的命令

```bash
Mon Jan 8 14:06:16 2024 us=151821 /sbin/ip link set dev tun0 up mtu 1500
# --remote地址还是要特殊对待的
Mon Jan 8 14:06:16 2024 us=155417 /sbin/ip addr add dev tun0 10.200.0.2/24 broadcast 10.200.0.255
Mon Jan 8 14:06:16 2024 us=157300 /sbin/ip route add 39.101.195.30/32 via 172.16.10.253
# 用两条优先级更高的路由覆盖了原有的默认路由
Mon Jan 8 14:06:16 2024 us=161527 /sbin/ip route add 0.0.0.0/1 via 10.200.0.1
Mon Jan 8 14:06:16 2024 us=163805 /sbin/ip route add 128.0.0.0/1 via 10.200.0.1
Mon Jan 8 14:06:16 2024 us=165804 /sbin/ip route add 172.16.0.0/24 via 10.200.0.1
Mon Jan 8 14:06:16 2024 us=167731 /sbin/ip route add 172.16.20.0/24 via 10.200.0.1
Mon Jan 8 14:06:16 2024 us=169664 /sbin/ip route add 172.16.30.0/24 via 10.200.0.1
```

客户端上的路由

```bash
[root@client1 ~]# ip route
# 子网前缀长度为1的路由
0.0.0.0/1 via 10.200.0.1 dev tun0 
# 原有的默认路由并没有被删除，只是被更高优先级的路由覆盖了
default via 172.16.10.253 dev eth0 
10.200.0.0/24 dev tun0 proto kernel scope link src 10.200.0.2 
# 到--remote的路由
39.101.195.30 via 172.16.10.253 dev eth0 
# 子网前缀长度为1的路由
128.0.0.0/1 via 10.200.0.1 dev tun0 
169.254.0.0/16 dev eth0 scope link metric 1002 
172.16.0.0/24 via 10.200.0.1 dev tun0 
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1 
172.16.20.0/24 via 10.200.0.1 dev tun0 
172.16.30.0/24 via 10.200.0.1 dev tun0 
```

在这样的配置下，vpn链路正常运行。
所有远程ssh连接、session manager连接都会断开，由于本地路由还在，可以使用同网段机器（本例中的app1）做跳板连接。
vnc不依靠机器里的路由配置，总是可用。

如果像实现更高的安全限制，可以通过配置`push "redirect-gateway def1 block"`
切断客户端本地LAN的访问。将与LAN内其他机器（非网关）的流量也路由到vpn服务器，这部分流量就被客户端的openvpn丢弃了。

客户端使用的命令

```bash
Mon Jan  8 14:26:06 2024 us=927138 /sbin/ip link set dev tun0 up mtu 1500
Mon Jan  8 14:26:06 2024 us=929808 /sbin/ip addr add dev tun0 10.200.0.2/24 broadcast 10.200.0.255
Mon Jan  8 14:26:06 2024 us=932487 /sbin/ip route add 39.101.195.30/32 via 172.16.10.253
Mon Jan  8 14:26:06 2024 us=937027 /sbin/ip route add 172.16.10.253/32 dev eth0
Mon Jan  8 14:26:06 2024 us=938588 /sbin/ip route add 0.0.0.0/1 via 10.200.0.1
Mon Jan  8 14:26:06 2024 us=940161 /sbin/ip route add 128.0.0.0/1 via 10.200.0.1
Mon Jan  8 14:26:06 2024 us=941619 /sbin/ip route add 172.16.0.0/24 via 10.200.0.1
Mon Jan  8 14:26:06 2024 us=943056 /sbin/ip route add 172.16.20.0/24 via 10.200.0.1
Mon Jan  8 14:26:06 2024 us=944465 /sbin/ip route add 172.16.30.0/24 via 10.200.0.1
Mon Jan  8 14:26:06 2024 us=945898 /sbin/ip route add 172.16.10.128/25 via 10.200.0.1
Mon Jan  8 14:26:06 2024 us=947378 /sbin/ip route add 172.16.10.0/25 via 10.200.0.1
```

客户端上的路由

```bash
[root@client1 ~]# ip route
# 用来覆盖默认路由的
0.0.0.0/1 via 10.200.0.1 dev tun0 
default via 172.16.10.253 dev eth0 
10.200.0.0/24 dev tun0 proto kernel scope link src 10.200.0.2 
# 到--remote的，保障vpn链路正常
39.101.195.30 via 172.16.10.253 dev eth0 
# 用来覆盖默认路由的
128.0.0.0/1 via 10.200.0.1 dev tun0 
169.254.0.0/16 dev eth0 scope link metric 1002 
172.16.0.0/24 via 10.200.0.1 dev tun0 
# 阻断到LAN内其他机器（非网关机器）的流量
172.16.10.0/25 via 10.200.0.1 dev tun0 
172.16.10.0/24 dev eth0 proto kernel scope link src 172.16.10.1 
# 阻断到LAN内其他机器（非网关机器）的流量
172.16.10.128/25 via 10.200.0.1 dev tun0 
# 到网关的，保障vpn链路正常
172.16.10.253 dev eth0 scope link 
172.16.20.0/24 via 10.200.0.1 dev tun0 
172.16.30.0/24 via 10.200.0.1 dev tun0 
```

在这样的配置下，vpn链路正常运行。
所有远程ssh连接、session manager连接都会断开，同网段机器（本例中的app1）也不能连接，唯一能用的就是vnc。