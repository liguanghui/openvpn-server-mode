resource "alicloud_vpc" "headquarters_office_vpc" {
  vpc_name   = "headquarters"
  cidr_block = "172.16.0.0/16"
}

resource "alicloud_vpc" "branch_office_1_vpc" {
  vpc_name   = "branch1"
  cidr_block = "172.16.0.0/16"
}

resource "alicloud_vpc" "branch_office_2_vpc" {
  vpc_name   = "branch2"
  cidr_block = "172.16.0.0/16"
}

resource "alicloud_vpc" "branch_office_3_vpc" {
  vpc_name   = "branch3"
  cidr_block = "172.16.0.0/16"
}

resource "alicloud_vpc_ipv4_gateway" "headquarters_office_vpc_igw" {
  ipv4_gateway_name = "headquarters-IGW"
  vpc_id            = alicloud_vpc.headquarters_office_vpc.id
  enabled           = "true"
}

resource "alicloud_vpc_ipv4_gateway" "branch_office_1_vpc_igw" {
  ipv4_gateway_name = "branch1-IGW"
  vpc_id            = alicloud_vpc.branch_office_1_vpc.id
  enabled           = "true"
}

resource "alicloud_vpc_ipv4_gateway" "branch_office_2_vpc_igw" {
  ipv4_gateway_name = "branch2-IGW"
  vpc_id            = alicloud_vpc.branch_office_2_vpc.id
  enabled           = "true"
}

resource "alicloud_vpc_ipv4_gateway" "branch_office_3_vpc_igw" {
  ipv4_gateway_name = "branch3-IGW"
  vpc_id            = alicloud_vpc.branch_office_3_vpc.id
  enabled           = "true"
}

resource "alicloud_route_table" "headquarters_office" {
  description      = "公司总部"
  vpc_id           = alicloud_vpc.headquarters_office_vpc.id
  route_table_name = "headquarters-office"
  associate_type   = "VSwitch"
}

resource "alicloud_route_table" "branch_office_1" {
  description      = "分支办公室1"
  vpc_id           = alicloud_vpc.branch_office_1_vpc.id
  route_table_name = "branch-office1"
  associate_type   = "VSwitch"
}

resource "alicloud_route_table" "branch_office_2" {
  description      = "分支办公室2"
  vpc_id           = alicloud_vpc.branch_office_2_vpc.id
  route_table_name = "branch-office2"
  associate_type   = "VSwitch"
}

resource "alicloud_route_table" "branch_office_3" {
  description      = "分支办公室3"
  vpc_id           = alicloud_vpc.branch_office_3_vpc.id
  route_table_name = "branch-office3"
  associate_type   = "VSwitch"
}

resource "alicloud_vswitch" "headquarters_office" {
  vswitch_name = "公司总部"
  cidr_block   = "172.16.0.0/24"
  vpc_id       = alicloud_vpc.headquarters_office_vpc.id
  zone_id      = "cn-zhangjiakou-c"
}

resource "alicloud_vswitch" "branch_office_1" {
  vswitch_name = "分支办公室1"
  cidr_block   = "172.16.10.0/24"
  vpc_id       = alicloud_vpc.branch_office_1_vpc.id
  zone_id      = "cn-zhangjiakou-c"
}

resource "alicloud_vswitch" "branch_office_2" {
  vswitch_name = "分支办公室2"
  cidr_block   = "172.16.20.0/24"
  vpc_id       = alicloud_vpc.branch_office_2_vpc.id
  zone_id      = "cn-zhangjiakou-c"
}

resource "alicloud_vswitch" "branch_office_3" {
  vswitch_name = "分支办公室3"
  cidr_block   = "172.16.30.0/24"
  vpc_id       = alicloud_vpc.branch_office_3_vpc.id
  zone_id      = "cn-zhangjiakou-c"
}

resource "alicloud_route_table_attachment" "headquarters_office" {
  vswitch_id     = alicloud_vswitch.headquarters_office.id
  route_table_id = alicloud_route_table.headquarters_office.id
}

resource "alicloud_route_table_attachment" "branch_office_1" {
  vswitch_id     = alicloud_vswitch.branch_office_1.id
  route_table_id = alicloud_route_table.branch_office_1.id
}

resource "alicloud_route_table_attachment" "branch_office_2" {
  vswitch_id     = alicloud_vswitch.branch_office_2.id
  route_table_id = alicloud_route_table.branch_office_2.id
}

resource "alicloud_route_table_attachment" "branch_office_3" {
  vswitch_id     = alicloud_vswitch.branch_office_3.id
  route_table_id = alicloud_route_table.branch_office_3.id
}

resource "alicloud_route_entry" "headquarters_office_gw" {
  route_table_id        = alicloud_route_table.headquarters_office.id
  destination_cidrblock = "0.0.0.0/0"
  nexthop_type          = "Ipv4Gateway"
  nexthop_id            = alicloud_vpc_ipv4_gateway.headquarters_office_vpc_igw.id
}

resource "alicloud_route_entry" "branch_office_1_gw" {
  route_table_id        = alicloud_route_table.branch_office_1.id
  destination_cidrblock = "0.0.0.0/0"
  nexthop_type          = "Ipv4Gateway"
  nexthop_id            = alicloud_vpc_ipv4_gateway.branch_office_1_vpc_igw.id
}
resource "alicloud_route_entry" "branch_office_2_gw" {
  route_table_id        = alicloud_route_table.branch_office_2.id
  destination_cidrblock = "0.0.0.0/0"
  nexthop_type          = "Ipv4Gateway"
  nexthop_id            = alicloud_vpc_ipv4_gateway.branch_office_2_vpc_igw.id
}
resource "alicloud_route_entry" "branch_office_3_gw" {
  route_table_id        = alicloud_route_table.branch_office_3.id
  destination_cidrblock = "0.0.0.0/0"
  nexthop_type          = "Ipv4Gateway"
  nexthop_id            = alicloud_vpc_ipv4_gateway.branch_office_3_vpc_igw.id
}

resource "alicloud_security_group" "headquarters_office_vpc_open_sg" {
  vpc_id              = alicloud_vpc.headquarters_office_vpc.id
  security_group_type = "normal"
  name                = "openSG"
  description         = "open to the world"
  inner_access_policy = "Accept"
}

resource "alicloud_security_group" "branch_office_1_vpc_open_sg" {
  vpc_id              = alicloud_vpc.branch_office_1_vpc.id
  security_group_type = "normal"
  name                = "openSG"
  description         = "open to the world"
  inner_access_policy = "Accept"
}

resource "alicloud_security_group" "branch_office_2_vpc_open_sg" {
  vpc_id              = alicloud_vpc.branch_office_2_vpc.id
  security_group_type = "normal"
  name                = "openSG"
  description         = "open to the world"
  inner_access_policy = "Accept"
}

resource "alicloud_security_group" "branch_office_3_vpc_open_sg" {
  vpc_id              = alicloud_vpc.branch_office_3_vpc.id
  security_group_type = "normal"
  name                = "openSG"
  description         = "open to the world"
  inner_access_policy = "Accept"
}

resource "alicloud_security_group_rule" "headquarters_office_allow_all_traffic" {
  type              = "ingress"
  ip_protocol       = "all"
  policy            = "accept"
  priority          = 1
  security_group_id = alicloud_security_group.headquarters_office_vpc_open_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "branch_office_1_allow_all_traffic" {
  type              = "ingress"
  ip_protocol       = "all"
  policy            = "accept"
  priority          = 1
  security_group_id = alicloud_security_group.branch_office_1_vpc_open_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "branch_office_2_allow_all_traffic" {
  type              = "ingress"
  ip_protocol       = "all"
  policy            = "accept"
  priority          = 1
  security_group_id = alicloud_security_group.branch_office_2_vpc_open_sg.id
  cidr_ip           = "0.0.0.0/0"
}

resource "alicloud_security_group_rule" "branch_office_3_allow_all_traffic" {
  type              = "ingress"
  ip_protocol       = "all"
  policy            = "accept"
  priority          = 1
  security_group_id = alicloud_security_group.branch_office_3_vpc_open_sg.id
  cidr_ip           = "0.0.0.0/0"
}

# 确定image
data "alicloud_images" "images_ds" {
  owners       = "system"
  name_regex   = "^centos_7"
  os_type      = "linux"
  most_recent  = true
  architecture = "x86_64"
}

# 确定key
data "alicloud_ecs_key_pairs" "work_mac_pro" {
  ids        = ["work-mac-pro"]
  name_regex = "work-mac-pro"
}

# 从2个交换机下分别启动一个ecs实例用作vpn主机
resource "alicloud_instance" "vpn_server" {
  security_groups            = [alicloud_security_group.headquarters_office_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.headquarters_office.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "vpn-server"
  private_ip                 = "172.16.0.1"
  host_name                  = "vpn-server"
}

resource "alicloud_instance" "branch_1_client" {
  security_groups            = [alicloud_security_group.branch_office_1_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.branch_office_1.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "client1"
  private_ip                 = "172.16.10.1"
  host_name                  = "client1"
}

resource "alicloud_instance" "branch_2_client" {
  security_groups            = [alicloud_security_group.branch_office_2_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.branch_office_2.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "client2"
  private_ip                 = "172.16.20.1"
  host_name                  = "client2"
}
resource "alicloud_instance" "branch_3_client" {
  security_groups            = [alicloud_security_group.branch_office_3_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.branch_office_3.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "client3"
  private_ip                 = "172.16.30.1"
  host_name                  = "client3"
}

data "aws_route53_zone" "main" {
  name = "lgypro.com"
}

resource "aws_route53_record" "openvpn_server" {
  zone_id = data.aws_route53_zone.main.zone_id
  name    = "openvpn"
  type    = "A"
  ttl     = "60"
  records = [alicloud_instance.vpn_server.public_ip]
}

resource "alicloud_route_entry" "headquarters_office_to_branch_1" {
  route_table_id        = alicloud_route_table.headquarters_office.id
  destination_cidrblock = "172.16.10.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.vpn_server.id
}

resource "alicloud_route_entry" "headquarters_office_to_branch_2" {
  route_table_id        = alicloud_route_table.headquarters_office.id
  destination_cidrblock = "172.16.20.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.vpn_server.id
}

resource "alicloud_route_entry" "headquarters_office_to_branch_3" {
  route_table_id        = alicloud_route_table.headquarters_office.id
  destination_cidrblock = "172.16.30.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.vpn_server.id
}

resource "alicloud_route_entry" "branch_office_1_to_headquarters" {
  route_table_id        = alicloud_route_table.branch_office_1.id
  destination_cidrblock = "172.16.0.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_1_client.id
}

resource "alicloud_route_entry" "branch_office_1_to_branch_office_2" {
  route_table_id        = alicloud_route_table.branch_office_1.id
  destination_cidrblock = "172.16.20.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_1_client.id
}

resource "alicloud_route_entry" "branch_office_1_to_branch_office_3" {
  route_table_id        = alicloud_route_table.branch_office_1.id
  destination_cidrblock = "172.16.30.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_1_client.id
}

resource "alicloud_route_entry" "branch_office_2_to_headquarters" {
  route_table_id        = alicloud_route_table.branch_office_2.id
  destination_cidrblock = "172.16.0.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_2_client.id
}

resource "alicloud_route_entry" "branch_office_2_to_branch_office_1" {
  route_table_id        = alicloud_route_table.branch_office_2.id
  destination_cidrblock = "172.16.10.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_2_client.id
}

resource "alicloud_route_entry" "branch_office_2_to_branch_office_3" {
  route_table_id        = alicloud_route_table.branch_office_2.id
  destination_cidrblock = "172.16.30.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_2_client.id
}

resource "alicloud_route_entry" "branch_office_3_to_headquarters" {
  route_table_id        = alicloud_route_table.branch_office_3.id
  destination_cidrblock = "172.16.0.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_3_client.id
}

resource "alicloud_route_entry" "branch_office_3_to_branch_office_1" {
  route_table_id        = alicloud_route_table.branch_office_3.id
  destination_cidrblock = "172.16.10.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_3_client.id
}

resource "alicloud_route_entry" "branch_office_3_to_branch_office_2" {
  route_table_id        = alicloud_route_table.branch_office_3.id
  destination_cidrblock = "172.16.20.0/24"
  nexthop_type          = "Instance"
  nexthop_id            = alicloud_instance.branch_3_client.id
}

resource "alicloud_instance" "headquarters_app" {
  security_groups            = [alicloud_security_group.headquarters_office_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.headquarters_office.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "app0"
  private_ip                 = "172.16.0.100"
  host_name                  = "app0"
}

resource "alicloud_instance" "branch_1_app" {
  security_groups            = [alicloud_security_group.branch_office_1_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.branch_office_1.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "app1"
  private_ip                 = "172.16.10.100"
  host_name                  = "app1"
}

resource "alicloud_instance" "branch_2_app" {
  security_groups            = [alicloud_security_group.branch_office_2_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.branch_office_2.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "app2"
  private_ip                 = "172.16.20.100"
  host_name                  = "app2"
}

resource "alicloud_instance" "branch_3_app" {
  security_groups            = [alicloud_security_group.branch_office_3_vpc_open_sg.id]
  vswitch_id                 = alicloud_vswitch.branch_office_3.id
  instance_charge_type       = "PostPaid"
  spot_strategy              = "SpotAsPriceGo"
  instance_type              = "ecs.t5-lc1m1.small"
  internet_charge_type       = "PayByTraffic"
  internet_max_bandwidth_out = 5
  system_disk_size           = max(20, data.alicloud_images.images_ds.images[0].size)
  key_name                   = data.alicloud_ecs_key_pairs.work_mac_pro.names[0]
  system_disk_category       = "cloud_efficiency"
  image_id                   = data.alicloud_images.images_ds.ids[0]
  instance_name              = "app3"
  private_ip                 = "172.16.30.100"
  host_name                  = "app3"
}
